#install.packages("recommenderlab")
#install.packages("stringr")
library("recommenderlab")
library("stringr")
library("Matrix")
library("plyr")

resturant_data$userID <- str_replace_all(resturant_data$userID, "U","")
resturant_data$userID <- as.numeric(resturant_data$userID)
resturant_data$placeID <- as.numeric(resturant_data$placeID)


as.integer(as.factor(resturant_data$placeID))
levels(as.factor(resturant_data$name))

sparse_ratings <- sparseMatrix(i = as.integer(as.factor(resturant_data$userID)), 
                               j = as.integer(((as.factor(resturant_data$name)))),
                               x = as.integer(as.factor(resturant_data$total_rating)), 
                               dimnames=list(levels(as.factor(resturant_data$userID)), 
                                             levels(as.factor(resturant_data$name))))
summary(sparse_ratings)

realRatingMatrix <- new("realRatingMatrix", data = sparse_ratings)
realRatingMatrix

class(realRatingMatrix)



similarity_users <- similarity(realRatingMatrix, method = "cosine" , which="users")
image(as.matrix(similarity_users),main = " User Similarity")
as.matrix(similarity_users)

class(similarity_users)
print(realRatingMatrix)



slotNames(realRatingMatrix)
class(realRatingMatrix@data)

vector_rating <- as.vector(realRatingMatrix@data)
unique(vector_rating)

table_ratings <- table(vector_rating)
table_ratings

vector_rating <- vector_rating[vector_rating != 0]

vector_rating <- factor(vector_rating)

qplot(vector_rating) + ggtitle("Distribution of Ratings")


#Estimating RMSE

model <- Recommender(realRatingMatrix, method = "UBCF", 
                     param=list(normalize = "center", method="Cosine", nn=5))

prediction <- predict(model, realRatingMatrix, type="ratings")

df <- as(prediction, "data.frame")



df$user <- paste("U", df$user, sep = "" )

colnames(df) <- c("userID", "placeID", "predicted_rating")
df %>% inner_join(user_profile)

df$predicted_rating <- round(df$predicted_rating,2)

#top - n resturant for each user
df <- df %>% group_by(userID,placeID,predicted_rating) %>% arrange(userID, desc(predicted_rating))
ddply(df, "userID", head, 3)

